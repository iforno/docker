#!/bin/bash

#by add this line below for mysql8 allow application connect to mysql using native_password
echo "default-authentication-plugin=mysql_native_password" >> /etc/my.cnf
#This setting to compatible with wordpress
echo "collation-server = utf8mb4_general_ci" >> /etc/my.cnf

mysqld --initialize 

mysqld -D

mysqladmin -u root -p$(grep 'temporary password' /var/log/mysqld.log | awk '{print $13}') password "$MYSQL_ROOT_PASSWORD" 

echo $MYSQL_ROOT_PASSWORD
echo $MYSQL_DB
echo $MYSQL_USER
echo $MYSQL_USER_PASSWORD


mysql -u root -p$MYSQL_ROOT_PASSWORD << EOF
create database $MYSQL_DB;
create user '$MYSQL_USER'@'%' identified by '$MYSQL_USER_PASSWORD';
grant all on $MYSQL_DB.* to '$MYSQL_USER'@'%';
EOF



exec $@
