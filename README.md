# docker

For Docker study

to start mysql image
1. build mysql image with command
docker build -t mysql:1.0 </path/to/Dockerfile>

2. run mysql image with custom variable input 
docker run -d -it --rm --name mysql --env MYSQL_ROOT_PASSWORD="1234567" --env MYSQL_DB="iforno1" --env MYSQL_USER="iforno1" --env MYSQL_USER_PASSWORD="iforno1"  mysql:1.0 /bin/bash

3. Build wordpress image
docker build -t httpd:1.0 </path/to/Dockerfile>

4. Start wordpress image
docker run -d -p 80:80 --name httpd httpd:1.0 

Next step
remove mysql container without lost data by
- create volume in container host	docker volume create dbdata
- check Volume location 		docker inspect dbdata
- mount this volume to mysql data folder /var/lib/mysql
docker run -d -it --rm -v dbdata:/var/lib/mysql --name mysql --env MYSQL_ROOT_PASSWORD="1234567" --env MYSQL_DB="iforno1" --env MYSQL_USER="iforno1" --env MYSQL_USER_PASSWORD="iforno1"  mysql:1.0 /bin/bash
  
