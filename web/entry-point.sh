#!/bin/bash
#check if any wordpress source code in /var/www/html or not
if [ -z "$(ls -A /var/www/html)" ] 
then
	wget https://wordpress.org/latest.zip
	unzip latest.zip -d /var/www/html/
	chown apache:apache -R /var/www/html/
fi

httpd -DFOREGROUND
